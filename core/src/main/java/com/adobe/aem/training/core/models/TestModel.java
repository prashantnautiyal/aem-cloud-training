package com.adobe.aem.training.core.models;

import com.day.cq.wcm.api.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.RequestAttribute;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TestModel {

    private static final Logger logger = LoggerFactory.getLogger(TestModel.class);

    @ScriptVariable
    private Page currentPage;

    @ScriptVariable
    private Resource resource;

    @ScriptVariable
    private ValueMap pageProperties;

    @SlingObject
    private ResourceResolver resourceResolver;

    @Self
    private SlingHttpServletRequest request;

    @RequestAttribute
    private String link;

    @PostConstruct
    protected void init() {
        logger.debug("Test model is working...");
        logger.debug("Current Page Path: {}", currentPage.getPath());
        logger.debug("Current Resource Path: {}", resource.getPath());
        logger.debug("Page Properties: {}", pageProperties.get("jcr:title", StringUtils.EMPTY));

        Resource cardResource = resourceResolver.getResource("/content/training/us/en/sling-model-practical/jcr:content/root/container/container/card");
        if (cardResource != null) {
            logger.debug("Card Resource Path: {}", cardResource.getPath());
        }
        logger.debug("Resource Resolver API: {}", resourceResolver.getUserID());

        logger.debug("Sling Http Request: {}", request.getResource().getPath());

        link = link.toUpperCase();

        logger.debug("Request Attribute: {}", link);
    }

    public String getLink() {
        return link;
    }
}
