package com.adobe.aem.training.core.models;

import com.adobe.aem.training.core.beans.Card;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ListOfCardModel {

    @ChildResource
    private Resource cards;

    protected List<Card> cardList;

    // Steps to implement the multi-field component in Sling Model
    // 1. Get the multi-field data in the Sling Model
    // 2. Store the data in Java bean class
    // 3. Send the data back to the HTL/Sightly and render it using Sly Attributes

    @PostConstruct
    protected void init() {
        cardList = new ArrayList<>();
        if (cards != null) {
            cards.listChildren().forEachRemaining(item -> {
                // We're retrieving the data from the item0, item1 and so on..
                ValueMap valueMap = item.getValueMap();
                String image = valueMap.get("cardimage", StringUtils.EMPTY);
                String name = valueMap.get("fullname", StringUtils.EMPTY);
                String description = valueMap.get("description", StringUtils.EMPTY);

                List<String> foodItemsList = new ArrayList<>();

                // Extract the Resource object for the nested multi-field
                Resource foodItemResource = item.getChild("foodItems");
                if(foodItemResource != null) {
                    foodItemResource.listChildren().forEachRemaining(child -> {
                        // Iterate the children and get the data in Java Bean
                        ValueMap childValueMap = child.getValueMap();
                        String foodItem = childValueMap.get("foodItem", StringUtils.EMPTY);
                        foodItemsList.add(foodItem);
                    });
                }

                // We have to store this data in Java bean
                Card card = new Card(image, name, description, foodItemsList);

                // Add the card to the list
                cardList.add(card);
            });
        }
    }

    public List<Card> getCardList() {
        return cardList;
    }
}
