package com.adobe.aem.training.core.services;

import com.adobe.aem.training.core.beans.User;
import org.osgi.annotation.versioning.ProviderType;

@ProviderType
public interface UserFormService {

    /**
     * Method to store the user data under the path: '/content/usergenerated/content'
     *
     * @param user User Object
     * @return {boolean}
     */
    boolean storeData(final User user);
}
