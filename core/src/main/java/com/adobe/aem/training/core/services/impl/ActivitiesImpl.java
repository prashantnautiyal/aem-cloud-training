package com.adobe.aem.training.core.services.impl;

import com.adobe.aem.training.core.services.Activities;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

@Component(service = Activities.class, immediate = true)
@Designate(ocd = ActivitiesImpl.Config.class)
public class ActivitiesImpl implements Activities {

    private static final Logger logger = LoggerFactory.getLogger(ActivitiesImpl.class);

    @ObjectClassDefinition(
            name = "Training Examples - Random Activity Config",
            description = "Configure Random Activity"
    )
    @interface Config {

        @AttributeDefinition(name = "List of activities", description = "activities", type = AttributeType.STRING)
        String[] randomactivities() default {"Running", "Cycling", "Swimming"};

        @AttributeDefinition(name = "Random Seed", description = "random seed", type = AttributeType.INTEGER)
        int randomseed() default 20;

    }

    private String[] randomActivities;
    private int randomSeed;

    private final Random random = new Random();

    /**
     * @return the name of a random activity
     */
    @Override
    public String getRandomActivity() {
        int randomIndex = random.nextInt(randomActivities.length);
        return randomActivities[randomIndex];
    }

    @Activate
    protected void activate(Config config) {
        this.randomActivities = config.randomactivities();
        this.randomSeed = config.randomseed();

        logger.debug("ActivitiesImpl Service activated with randomActivities [ {} ]",
                String.join(", ", this.randomActivities));
        logger.debug("Random Seed: {}", randomSeed);
    }

    @Deactivate
    protected void deactivate() {
        logger.debug("ActivitiesImpl Service Deactivated");
    }
}
