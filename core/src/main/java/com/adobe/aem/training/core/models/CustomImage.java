package com.adobe.aem.training.core.models;

import com.adobe.cq.wcm.core.components.models.Image;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.via.ResourceSuperType;

@Model(
        adaptables = SlingHttpServletRequest.class,
        adapters = Image.class,
        resourceType = "training/components/image",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class CustomImage implements Image {

    @Self
    @Via(type = ResourceSuperType.class)
    private Image delegate;

    @ValueMapValue
    private boolean useOriginal;

    @ValueMapValue
    private String fileReference;

    @Override
    public String getSrc() {
        return useOriginal ? fileReference : delegate.getSrc();
    }
}
