package com.adobe.aem.training.core.services.impl;

import com.adobe.aem.training.core.services.ContentStatistics;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.serviceusermapping.ServiceUserMapped;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.query.Query;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

@Component(
        reference = {
                @Reference(
                        service = ServiceUserMapped.class,
                        target = "(subServiceName=training-statistics)",
                        name = "training-statistics"
                )
        }
)
public class ContentStatisticsImpl implements ContentStatistics {

    private static final Logger log = LoggerFactory.getLogger(ContentStatisticsImpl.class);

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Override
    public int getAssetsCount(final ResourceResolver resourceResolver) {
        return queryAndCountAssets(resourceResolver);
    }

    @Override
    public int getAllAssetsCount() {
        final Map<String, Object> authInfo = Collections.singletonMap(
                ResourceResolverFactory.SUBSERVICE,
                "training-statistics"
        );
        try (ResourceResolver serviceResolver = resourceResolverFactory.getServiceResourceResolver(authInfo)) {
            return queryAndCountAssets(serviceResolver);
        } catch (LoginException e) {
            log.warn("Login exception: ", e);
        }
        return -1;
    }

    /**
     * Method to query the assets for a specific path and return the count
     *
     * @param resourceResolver ResourceResolver
     * @return {Integer}
     */
    private int queryAndCountAssets(ResourceResolver resourceResolver) {
        final String ASSETS_QUERY = "SELECT * FROM [dam:Asset] WHERE isdescendantnode(\"/content/dam/training\")";
        Iterator<Resource> resources = resourceResolver.findResources(ASSETS_QUERY, Query.JCR_SQL2);

        int count = 0;
        while (resources.hasNext()) {
            count++;
            resources.next();
        }
        log.info("User [ {} ] found [ {} ] assets", resourceResolver.getUserID(), count);
        return count;
    }

    @Activate
    protected void activate() {
        log.info("ContentStatisticsImpl Service Activated!");
    }
}
